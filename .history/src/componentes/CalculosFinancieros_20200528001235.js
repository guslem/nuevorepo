import React, { Component } from 'react';

export function calculaInteres(objeto) {
console.log("entre a la funcion interes")

    const {importeCalculo,    
        cftCalculado,    
        plazoCalculado,    
        interesCalculado, 
        teaCalculado,
        comCalculado,
        impCalculado,
        tasaAplicada  }= objeto
   
 
        const resultado = (importeCalculo * tasaAplicada / 365 * plazoCalculado /100)
    const ResultDecimal = resultado.toFixed(2)
console.log("salgo con:"  + ResultDecimal)
    return ResultDecimal;
}

export function calculaCostoFinanciero(objeto) {
    console.log("entre a la funcion cft")
    
        const {importeCalculo,    
            cftCalculado,    
            plazoCalculado,    
            interesCalculado, 
            teaCalculado,
            comCalculado,
            impCalculado,
            tasaAplicada  }= objeto
      // const interes = (importeCalculo * tasaAplicada / 365 * plazoCalculado /100)
      const interes = calculaInteres(objeto)  
      //const interesDecimal = interes.toFixed(2)

       const PorT = (((((interes + comCalculado + impCalculado)/ importeCalculo ))/plazoCalculado)*365)*100 ;
       const PortDecimal = PorT.toFixed(2)
       //=((((K21 + L21)/(J21-J7))/J31)*365)*100
       console.log("salgo con PorT:"  + PortDecimal)

       
       const basE = ( 1 / (1 - PortDecimal * (plazoCalculado / ( 365*100 ))));
       //=((1/(1-J33*(J31/(365*100))))^(365/J31)-1)*100
  


        console.log("salgo con basE:"  + basE)
       
        const potenciA = ( 365 / plazoCalculado) ;
       
        console.log("salgo con potenciA:"  + potenciA)
     
        const Calculo = (Math.pow(basE, potenciA )) - 1;
        console.log("salgo con Calculo:"  + Calculo)
        const resultado = Calculo * 100;
        const ResultDecimal = resultado.toFixed(2)
        console.log("salgo con:"  + ResultDecimal)
            return ResultDecimal;
        
    } 
    

    
export function calculaTea(objeto) {
    console.log("entre a la funcion tea")
    
        const {importeCalculo,    
            cftCalculado,    
            plazoCalculado,    
            interesCalculado, 
            teaCalculado,
            comCalculado,
            impCalculado,
            tasaAplicada  }= objeto
       const basE =(( 1/ (1 - ( tasaAplicada *  ( plazoCalculado / ( 365 * 100 ))))))
       //
       //((( 1/ (1 - ( tasaAplicada *  ( plazoCalculado / ( 365 * 100 )))))  ^ ( 365 / plazoCalculado)) - 1 ) * 100   

        console.log("salgo con basE tea:"  + basE)
       
        const potenciA = ( 365 / plazoCalculado) ;
       
        console.log("salgo con potenciA: tea"  + potenciA)
     
        const Calculo = (Math.pow(basE, potenciA )) - 1;
        console.log("salgo con Calculo: tea"  + Calculo)
        const resultado = Calculo * 100;
        const ResultDecimal = resultado.toFixed(2)
        console.log("salgo con: tea final"  + ResultDecimal)
            return ResultDecimal;
        
    } 
  
//	TEA =  ((( 1/ (1 - (Tasa Negociada *  ( PLAZO / ( 365 * 100 )))))  ^ ( 365 / PLAZO)) - 1 ) * 100  OK


//	CFTEA = {[(1/(1-d*(m/(df*100)))))^(df/m)]-1}*100                                                                        OK

    
export function calculaDigCuit(objeto) {

const formulaMulti = [5,4,3,2,7,6,5,4,3,2]

const {
    partA,
    partB,
    partC,
    resultado,
    cuitOK
  }  = objeto

const literal = partA + partB 

//const charArray = literal.toCharArray();
var i;
var operador;
var operando;
var parcial=0;
for (i = 0; i < literal.length; i++) {
    operador = parseInt(literal[i])
    operando = parseInt(formulaMulti[i])
    parcial = parcial + ( operador * operando)
    
    //console.log({parcial})
}

const resto =(parcial % 11 ) 
var ResultDecimal = parseInt(11 - resto)
if (ResultDecimal === 10) {
   ResultDecimal = 0
}


return ResultDecimal;

}


 
export function ValidaCmc7(objeto) {

    const formulaMulti = [1,3,9,7,1,3,9,7,1,3]
    const formulacta = [1,3,9,7,1,3,9,7,1,3,1]
    //ademas del conjunto de objetos hay que pasar la cantidad de elementos
     var i=0;
     var j=0;
     var k=0;
     var l=0;
     var m=0;
     var resto=0;
     var literal="";
     var operador=0;
     var operando=0;
     var parcial=0;
     var ResultDecimal=0;
     var ini=0;
     var fin=3;

     // 0 --16
      for (i=0; i< objeto.TotalInput.length; i++) {  
            
            // 0 --2 acumulo 3 campos
            //ini = 0 fin = 3
            for (j=ini; j < fin; j++) {          
                literal = literal + objeto.TotalInput[j].value
           //      console.log("acumula valor" + literal)
            } 
            console.log("acumula valor" + literal)
             // 0 --7  Recorremos campo
            for (k = 0; k < literal.length; k++) {    
                operador = parseInt(literal[k])
                operando = parseInt(formulaMulti[k])
                parcial = parcial + parseInt( operador * operando)
       //         console.log("Muestra valor" + parcial)
                //console.log({parcial})
            }
            console.log("resultado d1" + parcial)
            resto =parseInt(parcial % 10 ) 
            ResultDecimal = parseInt(10 - resto)
            if (ResultDecimal === 10) {
                ResultDecimal = 0
             }
         
           // nos movemos en el vector al digito 1
           //comparamos digito resultado vs digito ingresado
             i=i +3
             if (ResultDecimal !== objeto.TotalInput[i].value) {
                objeto.TotalInput[i].value = ResultDecimal
             }
          //--------------------------------------------------\\
          //iniciamos variables a reutilizar
          //nos movemos en el vector [b, s, c, d1,ch]
           i= i + 1
           literal=""
           parcial=0

           literal = literal + objeto.TotalInput[i].value

           for (l = 0; l < literal.length; l++) {    
            operador = parseInt(literal[l])
            operando = parseInt(formulacta[l])
            parcial = parcial + parseInt( operador * operando)
         //   console.log("Muestra valor d2" + parcial)
            //console.log({parcial})
           }
           console.log("resultado d2" + parcial)
           resto =parseInt(parcial % 10 ) 
           ResultDecimal = parseInt(10 - resto)
           if (ResultDecimal === 10) {
               ResultDecimal = 0
            }
        
            // nos movemos en el vector al digito 2
           //comparamos digito resultado vs digito ingresado
           //[b, s, c, d1,ch,d2]
           i=i + 1
           if (ResultDecimal !== objeto.TotalInput[i].value) {
              objeto.TotalInput[i].value = ResultDecimal
           }
          //--------------------------------------------------\\
          //iniciamos variables a reutilizar
          //nos movemos en el vector
          //[b, s, c, d1,ch,d2,cta]
          i=i + 1
          literal=""
          parcial=0

          literal = literal + objeto.TotalInput[i].value

           for (m = 0; m < literal.length; m++) {    
            operador = parseInt(literal[m])
            operando = parseInt(formulacta[m])
            parcial = parcial + parseInt( operador * operando)
          //  console.log("Muestra valor d3" + parcial)
            //console.log({parcial})
           }
           console.log("resultado d3" + parcial)
           resto =parseInt(parcial % 10 ) 
           ResultDecimal = parseInt(10 - resto)
           if (ResultDecimal === 10) {
               ResultDecimal = 0
            }
            
          //nos movemos al dg3 //[b, s, c, d1, ch, d2, cta, d3]
            i=i + 1
            if (ResultDecimal !== objeto.TotalInput[i].value) {
              objeto.TotalInput[i].value = ResultDecimal
           }
          //------------------ 
          //avanzamos al siguiente vector
          ini=i + 1
          fin = ini + 3 
          literal=""
          parcial=0


      }
    

    }