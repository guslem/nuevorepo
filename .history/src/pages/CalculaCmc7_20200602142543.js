import React, {Component} from 'react';
import { Title } from '../componentes/Title';
import PropTypes  from 'prop-types';
import { ValidaCmc7, querY } from '../componentes/CalculosFinancieros';
import { Principal } from './Principal';

export class CalculaCmc7 extends Component {


  state = ({
    CMC7:[]
  })
//agrego una fila  dinamicamente
_agregarFila = () =>{
  var TotalInput = document.querySelectorAll("input[type='text']")
 const idval = TotalInput.length  
   
const td55i = '<td width="55px" size="4" className="col1">'
const tdf   = '</td>'
const td65i = '<td width="65px" size="5" className="col1">'
const td35i = '<td width="35px" size="3" className="col1">'
const td85i = '<td width="85px" size="8" className="col1">'
const td135i = '<td width="130px"  className="col1">'

const td1= td55i + '<input id="' + (idval + 1)  + '" width="55px" size="4" type="text" maxlength="3"/>' + tdf
const td2= td55i + '<input id="' + (idval + 2)  + '" width="55px" size="4" type="text" maxlength="3"/>' + tdf
const td3= td65i + '<input id="' + (idval + 3) + '" width="65px" size="6" type="text" maxlength="4"/>' + tdf
const td4= td35i + '<input id="' + (idval + 4)  + '" width="55px" size="1" type="text" maxlength="1"/>' + tdf
const td5= td85i + '<input id="' + (idval + 5)  + '" width="85px" size="9" type="text" maxlength="8"/>' + tdf
const td6= td35i + '<input id="' + (idval + 6)  + '" width="55px" size="1" type="text" maxlength="1"/>' + tdf
const td7= td135i + '<input id="' + (idval + 7)  + '" width="150px" size="19" type="text" maxlength="11"/>' + tdf   
const td8= td35i + '<input id="' + (idval + 8) + '" width="55px" size="1" type="text" maxlength="1"/>' + tdf  
const td9= td135i + '<input id="' + (idval + 9)  + '" width="150px" size="19" type="text" maxlength="11"/>' + tdf   

  document.getElementById("TablaCmc7").insertRow(-1).innerHTML =`${td1} ${td2}
                                                                 ${td3} ${td4}
                                                                 ${td5} ${td6}
                                                                 ${td7} ${td8} ${td9}` ;

  }
  
  //Elimino una fila generada dinamicamente
  _eliminarFila = () =>{
    var table = document.getElementById("TablaCmc7");
    var rowCount = table.rows.length;
    //console.log(rowCount);
    
    if(rowCount <= 1)
      alert('No se puede eliminar el encabezado');
    else
      table.deleteRow(rowCount -1);
  } 
//obtengo el total de input de la pagina
_ValidaDigitoCmc7 = () => {
 //const TotalInput = document.getElementById("1")
 const TotalInput = document.querySelectorAll("input[type='text']")

 //Le paso el conjunto de Inputs, ademas tengo que pasar la cantidad para poder recorrer
this.setState({CMC7: ValidaCmc7({TotalInput})})
}
_volverAtras = ()=>{
  window.history.go(Principal)
// console.log("sali")
}

_limpiarFrom =() => {
    
  // obtenemos lo campos de tipo input 
        var elements = document.querySelectorAll("input[type='text']");
      // Por cada input field borramos el contenido
      for (var i = 0; i < elements.length; i++) {
        elements[i].value = ""};
     
  }
  
    render() {
     const { CMC7 } = this.state
        return (
      
           <div>
                    <Title>Calculo CMC7</Title>
              <div>
                  <div className="container">
                    <div>
                      <table id="TablaCmc7">
                        <thead >
                          <tr>
                                <th  className="titulotablain" width="60px">Bco.</th>
                                <th  className="titulotablain" width="60px">Suc.</th>
                                <th  className="titulotablain" width="72px">C.P.</th>
                                <th  className="titulotablain" width="42px">DV1</th>
                                <th  className="titulotablain" width="90px">Cheque</th>
                                <th  className="titulotablain"  width="42px">DV2</th>
                                <th  className="titulotablain"  width="150px">Cta. Libradora</th>
                                <th  className="titulotablain" width="42px">DV3</th>
                                <th  className="titulotablain"  width="150px">Doc. Librador</th>     
                          </tr>
                        </thead>
                        <tbody ></tbody>
                    
                        </table>
                      
                      <div>
                        <br></br>
                        <button className='button is-info is-outlined'
                         onClick={this._agregarFila}>Agregar Fila</button>
                        <button className='button is-info is-outlined'
                         onClick={this._eliminarFila}>Eliminar Fila</button>
                        <button className='button is-info is-outlined'
                         onClick={this._ValidaDigitoCmc7}>Validar</button>
                        <button  className='button is-info is-outlined'
                                  onClick={this._volverAtras}>Volver</button>
                        <button  className='button is-info'
                                  onClick={this._limpiarFrom}>Limpiar</button>
                                  
                      </div>
                    </div>
                  </div>
                </div>
        </div>

      );
    }
}