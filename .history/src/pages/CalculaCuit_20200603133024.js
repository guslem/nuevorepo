import React, {Component} from 'react';
import { Title } from '../componentes/Title';
import PropTypes  from 'prop-types';
import { calculaDigCuit } from '../componentes/CalculosFinancieros';
import { Principal } from './Principal';

export class CalculaCuit extends Component {

  static propTypes = {
    partA : PropTypes.string,
    partB : PropTypes.string ,
    partC : PropTypes.string,
    resultado: PropTypes.number,
    cuitOK: PropTypes.bool,
    cuitOKresult: PropTypes.bool,
    mensaje: PropTypes.string
}
state =({
    partA:"",
    partB:"",
    partC:"",
    resultado:0,
    cuitOK:false,
    cuitOKresult:false,
    mensaje:""

})

_handleChangePa = (e) => {

  this.setState ({partA : e.target.value})
 
}

_handleChangePb = (e) => {
  this.setState ({partB : e.target.value})

}

_handleChangePc = (e) => {

  this.setState ({partC : e.target.value})
}

_handleCalculaCuit = (e) =>{
  e.preventDefault()
  console.log("cuit:" + this.state.partA + this.state.partB + this.state.partC )
   this.setState({resultado : calculaDigCuit(this.state)})
   if (this.state.resultado === parseInt(this.state.partC)) {
    console.log("digito correcto:" + this.state.resultado)
    this.setState({cuitOKresult:false})
    this.setState({mensaje:"El digito es correcto"})
   } else {
    console.log("digito incorrecto:" + this.state.resultado)
    this.setState({cuitOKresult:true})
    this.setState({mensaje:"El digito es incorrecto"})
   }

   this.setState({cuitOK:false, })
   console.log("sali de validar cuit")
}


_limpiarFrom =() => {
    
  // obtenemos lo campos de tipo input 
        var elements = document.querySelectorAll("input[type='text']");
      // Por cada input field borramos el contenido
      for (var i = 0; i < elements.length; i++) {
        elements[i].value = ""};
      this.setState({mensaje:""})
  }
  
  _volverAtras = ()=>{
      window.history.go(Principal)
   // console.log("sali")
  }

  const mensajeUsuario =   cuitOKresult ? null : <h2>{this.state.mensaje}</h2>
render() {

  const {
    partA,
    partB,
    partC,
    resultado,
    cuitOK,
    cuitOKresult,
    mensaje
  } = this.state
   
      return (
              <div>
                    <div className="outer-line">
                         <div className="inner-line">
                             <Title>Validar Digito Cuit, Cuil, CDI</Title>
                        </div>
                    </div>
                  
                     <div>
                            <div className="container"> 
                              <input className="field" 
                                    id="id1"
                                    maxLength="2"
                                    placeholder="00" 
                                    size="1"
                                    type="text"  
                                    onChange={this._handleChangePa} > 
                              </input>
                              -
                              <input className="field" 
                                      id="id2"
                                      maxLength="8"
                                      placeholder="0000000000"
                                      size="15" 
                                      type="text" 
                                      onChange={this._handleChangePb}> 
                              </input>
                              -
                              <input  className="field" 
                                    id="id3"
                                    maxLength="1"
                                    placeholder="00" 
                                    size="1"
                                    type="text"
                                    onChange={this._handleChangePc}> 
                              </input>
                              </div>
                              <h3>Ingrese un cuit, cuil o cdi y presione validar</h3>
                      </div>
                      
                     
                      <div className="container"> 
                        
                     {mensajeUsuario}

                        <button  className="button is-success" 
                                onClick={this._handleCalculaCuit}>Validar</button> 
                        </div>
                    <div className="level-item">
                        <button  className='button is-info is-outlined'
                                  onClick={this._limpiarFrom}>                                     
                                  Limpiar</button>
                          <button  className='button is-info is-outlined'
                                  onClick={this._volverAtras}>                                  
                                  Volver</button>
                    </div>
                 </div>
                
              
              );
   }
}