import React, {Component} from 'react';
import { Title } from '../componentes/Title';
import PropTypes  from 'prop-types';
import { ValidaCmc7, querY } from '../componentes/CalculosFinancieros';
import { Principal } from './Principal';

export class CalculaCmc7 extends Component {


  state = ({
    CMC7:[]
  })
//agrego una fila  dinamicamente
_agregarFila = () =>{
  var TotalInput = document.querySelectorAll("input[type='text']")
  var idval = TotalInput.length  
var t1 = querY(idval)
  document.getElementById("tablaprueba").insertRow(-1).innerHTML =`${t1}`
   TotalInput = document.querySelectorAll("input[type='text']")
   idval = TotalInput.length  
 t1 = querY(idval)
  document.getElementById("tablaprueba").insertRow(-1).innerHTML +=`${t1}`



  //document.getElementById("tablaprueba").insertRow(-1).innerHTML =`${td3} ${td3} ${td4} ${td1}${td8} ${td1}${td12} ${td1}` ;

  }
  
  //Elimino una fila generada dinamicamente
  _eliminarFila = () =>{
    var table = document.getElementById("tablaprueba");
    var rowCount = table.rows.length;
    //console.log(rowCount);
    
    if(rowCount <= 1)
      alert('No se puede eliminar el encabezado');
    else
      table.deleteRow(rowCount -1);
  } 
//obtengo el total de input de la pagina
_ValidaDigitoCmc7 = () => {
  const TotalInput = document.querySelectorAll("input[type='text']")
  console.log(TotalInput.NodeList[0])
  this.setState({CMC7: ValidaCmc7({TotalInput})})
}

    render() {
     const { CMC7 } = this.state
        return (
      
           <div>
                    <Title>Calculo CMC7</Title>
              <div>
                  <div className="container">
                    <div>
                      <table id="tablaprueba">
                        <thead >
                          <tr>
                                <th width="55px">Bco.</th>
                                <th width="55px">Suc.</th>
                                <th width="65px">C.P.</th>
                                <th width="30px">DV1</th>
                                <th width="85px">Cheque</th>
                                <th width="30px">DV2</th>
                                <th width="130px">Cta. Libradora</th>
                                <th width="30px">DV3</th>
                                
                          </tr>
                        </thead>
                        <tbody ></tbody>
                    
                        </table>
                      
                      <div>
                        <br></br>
                        <button className='button is-info is-outlined' onClick={this._agregarFila}>Agregar Fila</button>
                        <button className='button is-info is-outlined' onClick={this._eliminarFila}>Eliminar Fila</button>
                        <button className='button is-info is-outlined' onClick={this._ValidaDigitoCmc7}>Validar</button>
                      </div>
                    </div>
                  </div>
                </div>
        </div>

      );
    }
}