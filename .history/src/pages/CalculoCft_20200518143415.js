import React, {Component} from 'react';
import {Title} from '../componentes/Title';
import PropTypes  from 'prop-types';
//import {Link} from 'react-router-dom'
import {calculaInteres, calculaCostoFinanciero, calculaTea, calculaCuit} from '../componentes/CalculosFinancieros';
import { Principal } from './Principal';
//import { Principal } from './Principal';


/*
si necesito usar los state en metodos necesito incluirlos en el constructos y
bindear los metodos que acceden a estos state
*/
export class CalculoCft extends Component {
 
 static propTypes = {
    importeCalculo : PropTypes.number ,
    cftCalculado : PropTypes.number ,
    plazoCalculado : PropTypes.number ,
    interesCalculado : PropTypes.number,
    teaCalculado : PropTypes.number,
    comCalculado : PropTypes.number,
    impCalculado : PropTypes.number,
    tasaAplicada : PropTypes.number,
    cftNOK : PropTypes.bool,
    inNOK :PropTypes.bool,
    teaNOK:PropTypes.bool
}
 
state =({importeCalculo   : 0,    
    cftCalculado     : 0,    
    plazoCalculado   : 0,    
    interesCalculado : 0,    
    teaCalculado     : 0,    
    comCalculado     : 0,    
    impCalculado     : 0,    
    tasaAplicada     : 0,
    cftNOK           :false,
    inNOK            :false,
    teaNOK           : false
})

 //Importe
_handleChangeImp  = (e) => {

   this.setState({importeCalculo:  Number.parseInt(e.target.value,10)})

}
//Plazo
_handleChangeplazo = (e) => {
 
    this.setState ({plazoCalculado : Number.parseInt(e.target.value,10)})

}
//Tasa
_handleChangetasa = (e) => {

  this.setState ({tasaAplicada : Number.parseFloat(e.target.value,10)})
   //this.setState ({tasaAplicada : tasaDecimal})
 
}
//comision
_handleChangecomi = (e) => {

    this.setState({comCalculado :  Number.parseInt(e.target.value,10)})
   
}
//Ipuestos
_handleChangeiva = (e) => {
    
   this.setState ({impCalculado :  Number.parseInt(e.target.value,10)})

}

//Calulo del cft
_calcularCft = (e) => {
   
     this.setState ({interesCalculado: calculaInteres(this.state)})
     this.setState ({cftCalculado: calculaCostoFinanciero(this.state)})
     this.setState({ teaCalculado: calculaTea(this.state)})
     
     this.setState ({cftNOK:true, inNOK:true, teaNOK:true})
}

_limpiarFrom =() => {
    
// obtenemos lo campos de tipo input 
      var elements = document.querySelectorAll("input[type='text']");
    // Por cada input field borramos el contenido
    for (var i = 0; i < elements.length; i++) {
      elements[i].value = ""};
    
}

_volverAtras = ()=>{
    window.history.go(Principal)
 // console.log("sali")
}

 


render() {

    const {importeCalculo,    
          cftCalculado,    
          plazoCalculado,    
          interesCalculado, 
          teaCalculado,
          comCalculado,
          impCalculado,
          tasaAplicada,
          cftNOK,
          inNOK,
          teaNOK      } = this.state
return (
    <div className="container"  >
             <div className="outer-line1">
                  <div className="inner-line1">
                  <Title>Calculo Interes y Cft</Title>
                   <small>Martin Lemos 2020</small>                            
                  </div>
              </div>       
                    <div className="notification">
                        <div className="field">
                            <div className="input1" width="50%">
                                <label>Importe  a Calcular--></label>
                                <input 
                                    className="field" 
                                    onChange={this._handleChangeImp}
                                    placeholder="Importe"
                                    type="text" 
                                />
                            </div>
                        </div>
                        <div className="field">
                            <div className="input1" width="50%">
                                <label>Plazo a Calcular--></label>
                                <input className="field"
                                    onChange={this._handleChangeplazo}
                                    placeholder="Plazo"
                                    type="text"
                                />
                            </div>
                        </div>
                        <div className="field">
                            <div className="input1" width="50%">
                                <label>Tasa Nominal Anual--></label>
                                <input  className="field"
                                        onChange={this._handleChangetasa}
                                        placeholder="TNA"
                                        type="text"
                                />
                            </div>
                        </div>
                        <div className="field">
                            <div className="input1" width="50%">
                                <label>Comision a aplicar--></label>
                                <input  className="field"
                                        onChange={this._handleChangecomi}
                                        placeholder="Comision"
                                        type="text" 
                                />
                            </div>
                        </div>
                        <div className="field">
                            <div className="input1" width="50%">
                                <label>Impuestos aplicados--></label>
                                <input className="field" 
                                    onChange={this._handleChangeiva}
                                    placeholder="Impuestos"
                                    type="text" 
                                />
                            </div>
                        </div>     
                        <nav className="level is-mobile">
                                <div className="level-item has-text-centered">
                                    <p className="subtitle">CFT</p>
                                    {{cftNOK}
                                    ?<p className="title">{this.state.cftCalculado}<small>%</small></p>
                                    :<p className="title">0<small>%</small></p>}
                                </div>    
                                <div className="level-item has-text-centered">    
                                    <p className="subtitle">TEA</p>
                                    {{teaNOK}
                                    ? <p  className="title">{this.state.teaCalculado}<small>%</small></p>
                                    :<p className="title">0<small>%</small></p>}
                                </div>
                
                                <div className="level-item has-text-centered">
                                    <p className="subtitle">Interes</p>
                                   {{inNOK}
                                    ? <p  className="title"><small>$</small>{this.state.interesCalculado}</p>
                                    :<p className="title"><small>$</small>0</p>}
                                </div>
                                
                        </nav>
                              <nav>
                                
                                     <button className="button is-success"
                                             onClick={this._calcularCft}>
                                            Calcular</button>
                            
                                    <div className="level-item">
                                            <button  className='button is-info is-outlined'
                                                        onClick={this._limpiarFrom}>                                     
                                             Limpiar</button>
                                            <button  className='button is-info is-outlined'
                                                        onClick={this._volverAtras}>                                  
                                             Volver</button>
                                    </div>
                               
                               </nav>
                    </div>
                
            
        </div>

        


);
}
}
