import React, { Component } from "react"


export class Botones extends Component {
    render() {
        const {valor}=this.props
      return (
        <div style={{height:70, width:70, fontSize:45}}>
            <button>{valor}</button>
            {this.props.children}
        </div>
           
      )
    }
  }