import React from 'react';
//import {Title} from './componentes/Title';
import 'bulma/css/bulma.css';
import {Principal} from './pages/Principal';
//import {Img} from './componentes/Img';
import {Switch, Route} from 'react-router-dom';
import './App.css';
import {CalculoCft} from './pages/CalculoCft';
//import {Siguiente} from './pages/Siguiente';
import {NotFound} from './pages/NotFound';

/*1ro la funcion APP no contiene render entonces cualquier cosa que invoque debe 
  tener una clase que tenga render
*/



function App() {
   console.log("Primer return App")
  return (

    <div className="App">
      
      <Switch>
      <Route exact path='/' component={Principal} />
      <Route path='/CalculoCft' component={CalculoCft}/>
      <Route component={NotFound}/>
      </Switch>
  
</div>
  );
 
}

export default App;
// <Route component={NotFound}/>
  /*
  //<Route path='/:id' component={Siguiente}/>
      //<Route path='/:id' component={CalculoCft}/>
       //<Route component={NotFound}/>
  */