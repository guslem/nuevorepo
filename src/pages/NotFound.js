import React from 'react';
import { Title, Subtitle } from '../componentes/Title';

export const NotFound = () => (
  
   <div>
       <Title>404!</Title>
       <Subtitle>Pagina no encontrada</Subtitle>
   </div>

)