import React, { Component } from "react";
//import {CalculoCft} from './CalculoCft';
import {Title} from '../componentes/Title';
import {Img} from '../componentes/Img';
import {Siguiente} from './Siguiente';
import { CalculoCft } from "./CalculoCft";
import { CalculaCuit } from "./CalculaCuit";
import { CalculaCmc7 } from "./CalculaCmc7";
//import {Title, Subtitle} from '../componentes/Title';


export class Principal extends Component {
  /*  constructor(props) {
        super(props);
   this.state = {accionCall: ''};
   this._handleResults = this._handleResults.bind(this);
   this._renderResult = this._renderResult.bind(this);    

}
*/
  
state = {primerLlamado:true, cftCall:false , cuitCall:false , cmc7Call:false }

 _renderResult  = (evento) =>{
  
    console.log("renderResult Principal " + evento)
    switch (evento) {
                  case 'CFT':
                      return    this.setState({cftCall :true, primerLlamado:false});
                      case 'CUIT':
                   //   return alert("Devolvio cuit ");
                      return    this.setState({cuitCall :true, primerLlamado:false});
                  case 'CMC7':
                      return this.setState({cmc7Call :true, primerLlamado:false});
                  default :
                       return alert("Devolvio otro ");
                   }
              
      }

      _renderCFT  (evento) {
        if (evento === 'CFT') {
               this.setState({cftCall :true})
        } 
        if (evento === 'CUIT') {
            this.setState({cuitCall :true})
        } 
        if (evento === 'CMC7') {
        } else {
           
            this.setState({cmc7Call :true})
        }
        console.log("renderResultCall Principal " + evento)
        
          }
        
    
      _handleResults  = (evento) => {
     
        console.log("handleResult " + evento) 
     this._renderResult(evento)
  
    }

    render() {
     
        console.log("Render Principal")
        
        

         if (this.state.primerLlamado) {
             console.log("primer ingreso return principal")
            return (
            <div>
               
                    
                    <div className="outer-line">
                        
                        <div className="inner-line">
                          <Title>Panel principal para calculos auxiliares</Title>
                          <Img/>
                        </div>  
                        <small>Martin Lemos 2020</small>                            
                     
                         
               
                      <Siguiente onAccion={this._handleResults}/>
                      </div>
                      <div className="outer-left"> 
                          <div className="inner-left">
                             
                          </div>
                      </div>
            </div>                                 
                    )
     }else { 
         return (
            <div>
                  {this.state.cftCall 
                     ? <CalculoCft/>
                     :<p></p>
                    }
                    {this.state.cuitCall 
                     ? <CalculaCuit/>
                     : <p></p>
                        }
                     {this.state.cmc7Call 
                     ? <CalculaCmc7/>
                     : <p></p>
                        }
             </div>
        );
    }
}
}
