import React, {Component} from 'react';

export class Siguiente extends Component {
 
    state ={
     inputAccion:false,
     contenidoForm:""

  }  

  _handleChange = (e) => {
    console.log("muestra seleccion " + e)
    this.setState({ inputAccion : true, contenidoForm : e})
  }

  
  _handleSubmit = (e) => {
    e.preventDefault()
      const { contenidoForm } = this.state
    
    //  console.log("submite ->" + contenidoForm)
      this.props.onAccion(contenidoForm)  
              }

  render() {
   // console.log("siguiente-1")
        return (
          <form className="miForm"  onSubmit={this._handleSubmit}>
           <div className="item">
           
                <button  className="button is-info is-outlined" 
                         onClick={() => {this._handleChange('CFT')}}  >CFT</button>
                <button  className="button is-info is-outlined" 
                         onClick={() => {this._handleChange('CUIT')}} >CUIT</button>
                <button  className="button is-info is-outlined" 
                         onClick={() => {this._handleChange('CMC7')}} >CMC7</button>

            </div>
           
            </form>
        );
    }
}